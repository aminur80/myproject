from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:car_id>/', views.detail, name='detail'),
    path('create/', views.create, name='create'),
    path('<int:car_id>/delete/', views.car_delete, name='delete'),
]