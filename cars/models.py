from django.db import models

class Category(models.Model):
    name = models.CharField(max_length=100)
    origincountry = models.CharField(max_length=50)
    def __str__(self):
        return self.name

class Car(models.Model):
    name = models.CharField(max_length=100)
    license = models.CharField(max_length=50)
    category = models.ForeignKey(Category, on_delete=models.DO_NOTHING)
    def __str__(self):
        return self.name




