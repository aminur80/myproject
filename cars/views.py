from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.shortcuts import get_object_or_404, render
from django.contrib import messages

from .models import Car, Category

def index(request):
    car_list = Car.objects.all()
    template = loader.get_template('cars/index.html')
    context = {
        'car_list' : car_list,
    }
    return HttpResponse(template.render(context, request))
def detail(request, car_id):
    car = get_object_or_404(Car, pk=car_id)
    return render(request, 'cars/detail.html', {'car':car})
def create(request):
     categories = Category.objects.all()
     context = {
        'categories' : categories,
     }
     if request.method == 'POST':
        if request.POST.get('name') and request.POST.get('license') and request.POST.get('category'):
            car=Car()
            car.name= request.POST.get('name')
            car.license= request.POST.get('license')
            car.category= get_object_or_404(Category, pk=request.POST.get('category'))
            car.save()

        messages.success(request, "Your car has been successfully created")
        return render(request, 'cars/create.html')
     return render(request,'cars/create.html', context)
def car_delete(request, car_id=None):
    car = get_object_or_404(Car, pk=car_id)
    if request.method == 'POST' and request.user.is_authenticated:
        car.delete()
        messages.success(request, "Car successfully deleted!")
        return HttpResponseRedirect("/cars/")

    context={
        'car':car,
    }

    return render(request, 'cars/car_delete.html', context)

